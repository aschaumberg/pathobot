# @pathobot documentation #

@pathobot is a Twitter bot that can search pathology cases, and is available at <https://twitter.com/pathobot>

@pathobot and its author @schaumberg_a (Andrew Schaumberg) are not doctors and do not provide medical advice.

## Frequently asked questions ##

### 1. How can I start using @pathobot? ###

Send @pathobot a Direct Message.  We'll ask for your name, institution, etc.  When that is done, you can use @pathobot.

Please consider sharing your data with @pathobot, which is new and needs more data to improve.  We may add new users slowly at first, depending on the feedback.

For more on pathology social media best practices and uses, please see:

* Crane and Gardner 2016 <https://www.ncbi.nlm.nih.gov/pubmed/27550566> "Pathology Image-Sharing on Social Media: Recommendations for Protecting Privacy While Motivating Education."

* Gardner and Allen 2019 <https://www.ncbi.nlm.nih.gov/pubmed/30132683> "Keep Calm and Tweet On: Legal and Ethical Considerations for Pathologists Using Social Media."

* Nix et al 2018 <https://www.ncbi.nlm.nih.gov/pubmed/29788115> "Neuropathology Education Using Social Media."

* Dirilenoglu and Önal 2019 <https://www.ncbi.nlm.nih.gov/pubmed/30820234> "A welcoming guide to social media for cytopathologists: Tips, tricks, and the best practices of social cytopathology."

### 2. I'll share a case on Twitter.  How can I use @pathobot to find similar cases to this? ###

Mention "@pathobot" in your tweet for your case.  Be careful to not mention "@pathbot", which is not our Twitter account.  We're "@pathobot".  Be sure to tweet at least one image that is at least 512 pixels wide and at least 512 pixel high, because @pathobot needs this much image information at a minimum.

For best results, include DDx terms (e.g. adenocarcinoma) and tissue type (e.g. #gynpath).

### 3. I'm interested in a case already on Twitter.  How can I use @pathobot to find similar cases to this? ###

There are two methods to do this.

The first method is to mention "@pathobot" in your reply to the case, like this: <https://twitter.com/schaumberg_a/status/1131660403841347584>

Alternatively, if you would prefer to not reply, the second method is to tweet "@pathobot LINK_TO_TWEET".

For example, Dr Rosado tweeted a case here: <https://twitter.com/KathiarRosado/status/1120821389316034560>

I wanted to find cases similar to Dr Rosado's case, so my tweet (<https://twitter.com/schaumberg_a/status/1121521048720113667>) was exactly "Naively checking for similar cases in data (probably not accurate): @pathobot https://twitter.com/KathiarRosado/status/1120821389316034560".  Twitter may hide the link "https://twitter.com/KathiarRosado/status/1120821389316034560" when displaying my tweet.  Importantly, my tweet follows the "@pathobot LINK_TO_TWEET" format, i.e. "@pathobot https://twitter.com/KathiarRosado/status/1120821389316034560".  This allows @pathobot to know to search.

@pathobot then searched for cases that were similar to Dr Rosado's cases and replied: <https://twitter.com/pathobot/status/1121521099953537032>.  Serendipitously, search result #2 was similar to Dr Rosado's case, according to Dr Pastrian: <https://twitter.com/DraEosina/status/1121682679575928832>.  Search result #2 is an adenoma: <https://twitter.com/KathiarRosado/status/974437437635289090>.  Later, Dr Rosado said her case <https://twitter.com/KathiarRosado/status/1120821389316034560> was also an adenoma <https://twitter.com/KathiarRosado/status/1120821891898511361>.  It was probably luck for us to find an adenoma when the original case was also an adenoma -- many search results may not be so accurate.  Feedback and cases from pathologists can help us improve.

### 4. How do I focus @pathobot search, so only cases with a specific diagnosis are in the search results? ###

Say "require ABC" in your search, to only allow search results with diagnosis ABC (to review how to search with pathobot, please see #3 above).

For example, the following tweet is a search that mentions @pathobot and "require teratoma": <https://twitter.com/schaumberg_a/status/1146621605713502208>.  Then @pathobot only gave teratoma search results: <https://twitter.com/pathobot/status/1146621821032259584>

As another example, the follow tweet is a pathobot search with "require glom": <https://twitter.com/schaumberg_a/status/1138444762237800449>.  "Glom" is essentially shorthand for "glomus tumor", because few other related diagnoses would have the text "glom".  Indeed, pathobot returned glomus tumor search results: <https://twitter.com/pathobot/status/1138445100693020672>

Use quotes for a multi-word requirement, such as "solid pseudopapillary": <https://twitter.com/schaumberg_a/status/1145779732534968320>.  Solid pseudopapillary neoplasm is a rare entity, so rare that we did not have it in the Twitter search results <https://twitter.com/pathobot/status/1145779905571037184> but we did have it in the PubMed search results <https://twitter.com/pathobot/status/1145780438763560960>

In an advanced example, we can see multiple requirements and alternative requirements in a search: <https://twitter.com/schaumberg_a/status/1145807798611140608>.  This search requires that all search results (1) include "colon", (2) include "polyp", and (3) include either "hamartoma" or "inflam".  Indeed, pathobot gave results that were either hamartomatous polyps or inflammatory polyps, of the colon: <https://twitter.com/pathobot/status/1145808068380418049>

Please note "require" is very strict and has no common sense.  For instance, require "mucinous spindle cell carcinoma" will not match tweets with the text "spindle cell mucinous carcinoma", because the word order differs.  To flexibly match a variety of word orders, say require mucinous and require "spindle cell" and require carcinoma.  That's three requirements all in one tweet to search.  Generally speaking, unless you are only interested in a specific entity for search, it is best to not use "require" at first.  If the results of your first search are not specific enough, then consider "require" in your second search. 

### 5. How does @pathobot work? ###

We maintain a database of cases from collaborating pathologists.

Then on demand, @pathobot compares any case of interest to everything in this database, reporting a short list of the most similar cases.  This is the search result list.

Please bear in mind that @pathobot is very new, so @pathobot may not work very well yet.

### 6. How may I interpret @pathobot's search results? ###

Ideally, the #1 search result is the most similar case -- more similar than #2 or #8.  Being most similar, #1 ideally has the most similar diagnosis and histology, but in reality #2 or some other search result may be most similar, e.g. <https://twitter.com/DraEosina/status/1121682679575928832>.

Similarity depends both on tweet text and on image content.  If "glioblastoma" is mentioned in a search tweet, then search results will also tend to mention "glioblastoma".  If "#gipath" is in a search tweet, search results will tend to be "#gipath" too.  An image of malignant disease will tend to be more similar to other images that depict malignancy.  Benign images will tend to be more similar to other benign images.  

Image search may work less well for overstained/understained slides, and may work less well if the image is not well illuminated.  The search method is imperfect, but performance should improve as more cases/data are shared with us.  Currently, we consider search to be "successful" if any search result is similar, or if the search results start a pathology conversation.

If all DDx terms are not given, @pathobot may give biased results, favoring cases that mention the limited diagnostic terms given.  For example, the following case asks if a polyp is an adenoma: <https://twitter.com/parthisandy/status/1122735828600418304>  The search results are enriched for polyp and adenoma cases <https://twitter.com/pathobot/status/1122915227119554565>, but not much else.  If there is suspicion that this is a carcinoma in situ, inflammation, or something else -- mentioning all those possibilities gives @pathobot the best chance of finding the truly most similar cases.

### 7. Is pathologist permission obtained before including cases in the @pathobot database of cases? ###

Yes, we obtain written consent from the pathologist first.  We treat these consenting pathologists as collaborators.  Only after we obtain pathologist consent do we include their cases in our database.  Therefore, @pathobot search results are limited to cases from consenting collaborating pathologists.  We believe having many collaborating pathologists is essential for @pathobot to ever be useful.

### 8. Is pathologist permission required before searching for similar cases, especially if the case belongs to someone else? ###

We believe @pathobot search does not differ much in principle from a Google/Bing search, or a Google/Bing image search, though pathobot is pathology-specific.  If a non-collaborating pathologist shares a case publicly on Twitter, and you wish to use @pathobot to search for cases similar to that, we do not believe that you are required to obtain permission from the non-collaborating pathologist to use @pathobot for this search.  However, if you have questions, please ask us and the non-collaborating pathologist first.

### 9. I notice @pathobot has a problem.  What should I do? ###

Please politely respond to @pathobot, and mention @schaumberg_a, indicating what is wrong.  If appropriate, send a Direct Message instead.  We'll do our best to fix it, and really appreciate your feedback!

@pathobot is new, and needs more data to improve. We expect that, initially, many @pathobot searches may produce cases that do not in fact have similar diagnoses, histology, etc. Please flag those searches for us, so we can use them to improve in the future. And we would be eager to hear your ideas about other ways that @pathobot can help you.

### 10. How do I cite @pathobot or learn more? ###

Please cite the following preprint, which will soon mention @pathobot:

Schaumberg et al 2018 "Large-Scale Annotation of Histopathology Images from Social Media" <https://doi.org/10.1101/396663>

We would like to acknowledge the critical contribution of many pathologists who have helped us in this project. Without them, this project would not have been possible. Our hope is that through their contribution -- and yours -- we can help people worldwide.
